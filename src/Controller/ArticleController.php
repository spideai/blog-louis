<?php

namespace App\Controller;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\CommentsFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article/{id}", name="ArticleController")
     */ 
    public function index(string $id, Request $request): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }
        $article = $this->getDoctrine()
        ->getRepository(Article::class)
        ->find($id);
        if (!$id) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $commentId = $request->request->get("delete_comment");

        if ($commentId != null) {
            $commentToDelete = $this->getDoctrine()
                ->getRepository(Comment::class)
                ->find($commentId);
            if ($commentToDelete != NULL) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($commentToDelete);
                $entityManager->flush();
                $request->request->remove("delete_comment");
            } 
        };

        $articleId = $request->request->get("delete_article");
        if ($articleId != null) {
            $articleToDelete = $this->getDoctrine()
                ->getRepository(Article::class)
                ->find($articleId);
            if ($articleToDelete != NULL) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($articleToDelete);
                $entityManager->flush();
                $request->request->remove("delete_article");
                return $this->redirectToRoute('default');
            }
        };

        $comments = $this->getDoctrine()
            ->getRepository(Comment::class)
            ->findBy(['article_id' => [$article]]);

        $comment = new Comment();
        $commentForm = $this->createForm(CommentsFormType::class, $comment);
        $commentForm->handleRequest($request);

        $comment->setDate(new \DateTime());
        $currentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(["email" => $this->getUser()->getUsername()]);
        $comment->setUserId($currentUser);
        $comment->setState(1);
        $comment->setArticleId($article);


        if ($commentForm->isSubmitted() && $commentForm->isValid()) 
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();
           
            // unset($comment);
            // unset($commentForm);
            // $comment = new Comment();
            // $commentForm = $this->createForm(CommentsFormType::class, $comment);
        };

        
        return $this->render('article/index.html.twig', [
            'comments' => $comments,
            'commentForm' => $commentForm->createView(),
            'article' => $article,
        ]);
    }

    /**
     * @Route("/comment/{id}", name="deleteComment", methods={"DELETE", "GET"})
     */ 

    public function deleteComment(string $id, Request $request)
    {
        
    }

}


