<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=article::class, mappedBy="category")
     */
    private $articles_id;

    public function __construct()
    {
        $this->articles_id = new ArrayCollection();
    }

    public function __toString() 
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|article[]
     */
    public function getArticlesId(): Collection
    {
        return $this->articles_id;
    }

    public function addArticlesId(article $articlesId): self
    {
        if (!$this->articles_id->contains($articlesId)) {
            $this->articles_id[] = $articlesId;
            $articlesId->setCategory($this);
        }

        return $this;
    }

    public function removeArticlesId(article $articlesId): self
    {
        if ($this->articles_id->removeElement($articlesId)) {
            // set the owning side to null (unless already changed)
            if ($articlesId->getCategory() === $this) {
                $articlesId->setCategory(null);
            }
        }

        return $this;
    }
}
